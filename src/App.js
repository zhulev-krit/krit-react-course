import './App.css';
import './styles.css';
import foto from './images/spyro.png'
import ava from './images/mask2.jpg';
import {Switch,Route,NavLink} from 'react-router-dom'

const arr = ['Страница', 'Новости', 'Настройки'];

function App() {
    return (
        <div className="App">
            <div className="wrapper">
                <div className="sidebar">
                    <div className="sidebar__imgBlock">
                        <img className="sidebar__avatar" src={ava} alt="Avatar"/>
                    </div>
                    <div className="sidebar__listContainer">
                        <ul className="sidebar__list">
                            {arr.map((item,index)=> {
                                return(
                                    <li><NavLink className="link" to={`h${index+1}`}>{item}</NavLink></li>
                                )
                            })}
                        </ul>
                    </div>
                </div>
                <div className="mainContent">
                    <div className="header">
                        <div className="header__list">
                            <ul className="header__listItems">
                                {arr.map((item,index)=> {
                                    return (
                                    <li><NavLink className="link" to={`h${index+1}`}>{item}</NavLink></li>
                                    )
                                })}
                            </ul>
                        </div>
                    </div>
                    <div className="content">
                        <Switch>
                            <Route path={'/h1'} render={() =>
                                <div className="content__itemWrapper">
                                    <div className="content__item">
                                        <p className="content__itemText">
                                            <h2>Страница</h2>
                                        </p>
                                    </div>
                                </div>}/>
                            <Route path={'/h2'} render={()=>
                                <div className="content__itemWrapper">
                                    <div className="content__item">
                                        <p className="content__itemText">
                                            <h2>Новости</h2>
                                        </p>
                                    </div>
                                </div>}/>
                            <Route path={'/h3'} render={()=>
                                <div className="content__itemWrapper">
                                    <div className="content__item">
                                        <p className="content__itemText">
                                            <h2>Настройки</h2>
                                        </p>
                                    </div>
                                </div>
                            }/>
                        </Switch>
                       {/* <div className="content__itemWrapper">
                            <div className="content__item">
                                <img className="content__itemImage" src={foto} alt="Picture"/>
                            </div>
                        </div>
                        <div className="content__itemWrapper">
                            <div className="content__item">
                                <p className="content__itemText">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                                    Asperiores dolores dolorum ducimus ea earum excepturi illo incidunt labore laborum,
                                    maiores maxime molestiae neque nihil non perspiciatis possimus quaerat quas rem
                                    saepe sunt!
                                    Architecto consequuntur, delectus doloremque eum exercitationem facere illum
                                    impedit, ipsa
                                    odit,
                                    quas ullam voluptate voluptatem!
                                    Adipisci alias, dignissimos in minus qui quisquam quo recusandae rerum sint, ullam
                                    veritatis.
                                </p>
                            </div>
                        </div>
                        <div className="content__itemWrapper">
                            <div className="content__item">
                                <img className="content__itemImage" src={foto} alt="Picture"/>
                            </div>
                        </div>*/}
                    </div>
                </div>
            </div>
        </div>
    );
}

export default App;
